const got = require("./data-1_1");

function nameWithA() {
  try {
    if (typeof got == "object") {
      const houseData = got.houses;

      const houseDataResult = houseData
        .reduce((acummulator, current) => {
          const peopelData = current["people"];

          acummulator.push(
            peopelData.reduce((storeNames, namesData) => {
              if (
                namesData.name.includes("A") ||
                namesData.name.includes("a")
              ) {
                storeNames.push(namesData.name);
              }
              return storeNames;
            }, [])
          );

          return acummulator;
        }, [])
        .flat();

      return houseDataResult;
    } else {
      throw new Error("Data is not exported correctly");
    }
  } catch (error) {
    console.log(error);
  }
}
module.exports = nameWithA;
