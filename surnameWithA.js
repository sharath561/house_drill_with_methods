const got = require("./data-1_1");

function surnameWithA() {
  try {
    if (typeof got == "object") {
      let houseData = got.houses;

      const houseDataResult = houseData
        .reduce((acuumulator, current) => {
          const peopleData = current["people"];

          acuumulator.push(
            peopleData.reduce((storeNames, names) => {
              if (names["name"].split(" ")[1][0] == "A") {
                storeNames.push(names["name"]);
              }

              return storeNames;
            }, [])
          );

          return acuumulator;
        }, [])
        .flat();

      return houseDataResult;
    } else {
      throw new Error("Data is not exported correctly");
    }
  } catch (error) {
    console.log(error.message);
  }
}

module.exports = surnameWithA;
