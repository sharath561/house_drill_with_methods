const got = require("./data-1_1");

function everyone() {
  try {
    if (typeof got == "object") {
      let houseData = got.houses;

      const houseDataResult = houseData
        .reduce((accumulator, current) => {
          const peopleData = current["people"];

          accumulator.push(
            peopleData.reduce((storeNames, namesData) => {
              storeNames.push(namesData["name"]);

              return storeNames;
            }, [])
          );

          return accumulator;
        }, [])
        .flat();
      return houseDataResult;
    } else {
      throw new Error("Data is not exported correctly");
    }
  } catch (error) {
    console.log(error.message);
  }
}

module.exports = everyone;
