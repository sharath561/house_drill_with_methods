const got = require("./data-1_1");

function nameWithS() {
  try {
    if (typeof got == "object") {
      let houseData = got.houses;

      const houseDataResult = houseData
        .reduce((accumulator, current) => {
          const peopleData = current.people;

          accumulator.push(
            peopleData.reduce((storeNames, peopleName) => {
              if (
                peopleName.name.includes("s") ||
                peopleName.name.includes("S")
              ) {
                storeNames.push(peopleName.name);
              }
              return storeNames;
            }, [])
          );

          return accumulator;
        }, [])
        .flat();

      return houseDataResult;
    } else {
      throw new Error("data is not exported correctly");
    }
  } catch (error) {
    console.log(error);
  }
}
module.exports = nameWithS;
