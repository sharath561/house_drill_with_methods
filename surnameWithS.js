const got = require("./data-1_1");

function surnameWithS() {
  try {
    if (typeof got == "object") {
      let houseData = got.houses;

      let houseDataResult = houseData
        .reduce((accumulator, current) => {
          let nameData = current.people;

          accumulator.push(
            nameData.reduce((storeNames, names) => {
              if (names["name"].split(" ")[1][0] == "S") {
                storeNames.push(names["name"]);
              }

              return storeNames;
            }, [])
          );

          return accumulator;
        }, [])
        .flat();

      return houseDataResult;
    } else {
      throw new Error("data is not exported correctly");
    }
  } catch (error) {
    console.log(error.message);
  }
}
module.exports = surnameWithS;
