const got = require("./data-1_1");

function countAllPeople() {
  try {
    if (typeof got == "object") {
      let houseData = got.houses;

      let houseDataResult = houseData.reduce((accumulator, current) => {
        accumulator += current["people"].length;
        return accumulator;
      }, 0);
      return houseDataResult;
    } else {
      throw new Error("data is not exported correctly");
    }
  } catch (error) {
    console.log(error.message);
  }
}

module.exports = countAllPeople;
