const got = require("./data-1_1");

function peopleNameOfAllHouses() {
  try {
    if (typeof got == "object") {
      let houseData = got.houses;

      const houseDataResult = houseData.reduce((accumulator, current) => {
        const peopleData = current["people"];

        accumulator.push(
          peopleData.reduce((storeNames, namesData) => {
            if (storeNames[current.name]) {
              storeNames[current.name].push(namesData["name"]);
            } else {
              storeNames[current.name] = [namesData["name"]];
            }

            return storeNames;
          }, {})
        );

        return accumulator;
      }, []);

      return houseDataResult;
    } else {
      throw new Error("Data is not exported correctly");
    }
  } catch (error) {
    console.log(error);
  }
}
module.exports = peopleNameOfAllHouses;
