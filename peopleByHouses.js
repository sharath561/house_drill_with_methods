const got = require("./data-1_1");

function peopleByHouses() {
  try {
    if (typeof got == "object") {
      let housesData = got.houses;

      const houseDataResult = housesData.reduce((accumulator, current) => {
        //console.log(current.people.length)
        accumulator[current.name] = current.people.length;
        return accumulator;
      }, {});

      return houseDataResult;
    } else {
      throw new Error("data is not exported correctly");
    }
  } catch (error) {
    console.log(error.message);
  }
}

module.exports = peopleByHouses;
